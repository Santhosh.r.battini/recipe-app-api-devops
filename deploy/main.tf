terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket         = "recipe-app-api-devops-tf-state"
    key            = "recipe-app-tf-state"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }

}

provider "aws" {
  region  = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {

}


